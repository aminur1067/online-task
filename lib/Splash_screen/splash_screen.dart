import 'dart:async';

import 'package:flutter/material.dart';


import '../home_page.dart';

class SpashScreen extends StatefulWidget {
  const SpashScreen({Key? key}) : super(key: key);

  @override
  State<SpashScreen> createState() => _SpashScreenState();
}

class _SpashScreenState extends State<SpashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds:5), (){
      Navigator.push(context , MaterialPageRoute(builder: (context) =>HomePage()));
    });
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(

      body: Container(
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FittedBox(
              child: Image.asset('assets/images/ui22.jpg'
               ,height: 615,
              width: 360,
              fit: BoxFit.cover,),
            ),

          ],),

      ),

    ));
  }
}
