import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:hexcolor/hexcolor.dart';

import 'Drawer_page/drawer_page.dart';
import 'Drawer_page/menu_screen.dart';
import 'Product_details_page/product_details_page.dart';

class HomePage extends StatefulWidget {

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {



  @override
  Widget build(BuildContext context) {

    double hight=MediaQuery.of(context).size.height;
    double width=MediaQuery.of(context).size.width;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Scaffold(

          bottomNavigationBar: CurvedNavigationBar(

            height: 50,
            backgroundColor: Colors.grey.shade100,
            buttonBackgroundColor: Colors.blue,



            items:   [

              Icon(Icons.home,size: 30,),
            Icon(Icons.favorite_border,size: 30,),
            Icon(Icons.lock_outlined,
            size: 30,
            color: Colors.blue,),
              Icon(Icons.notifications,size: 30,),
              Icon(Icons.person,size: 30,),
            // Icon(Icons.home),
            // Icon(Icons.home)

          ],

          ),
          backgroundColor:  HexColor ('F8F9FB'),
          // appBar: AppBar(
          //   backgroundColor: Colors.grey.shade100,
          //
          // ),

          body: Container(
            margin: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Stack(
              children: [ ListView(
                scrollDirection: Axis.vertical,
                children: [


                  Padding(padding: EdgeInsets.all(10)),


                  //header


                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: (){
                            print('object');
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>Draw2er()));
                          },
                          child: Center(
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              child: Container(
                                child: Center(
                                  child: IconButton(
                                    onPressed: ()=>ZoomDrawer.of(context)!.toggle(),
                                    icon: Center(child: Icon(Icons.apps_sharp)),),
                                ),
                                height: 20,
                                width: 20,
                              ),
                            ),
                          )
                      ),
                      Column(
                        children: [
                          Container(
                            child: Text('Store Location'),
                          ),
                          Row(
                            children: [
                              Container(
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.red,
                                ),
                              ),
                              Container(
                                child: Text('Mondilibug,Sylhet'),
                              ),
                            ],
                          )
                        ],
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Container(
                          height: 20,
                          width: 20,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  // search


                  Container(
                    width: 309,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(60)),
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                          icon: Icon(Icons.search),
                          hintText: "Loking for shoes",
                          hintStyle:
                          TextStyle(textBaseline: TextBaseline.alphabetic)),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  FittedBox(
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {},
                          child: Container(
                            width: 83,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(60)),
                            ),
                            child: Container(
                              height: 20,
                              decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius:
                                BorderRadius.all(Radius.circular(60)),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Container(
                                    height: 25,
                                    child: CircleAvatar(
                                      radius: 20,
                                      child: ClipOval(
                                        child: Image.network(
                                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7SEu_Vjo-8YDvR7Xyyasp9QBP_wLr7TqeEQ&usqp=CAU',
                                          fit: BoxFit.cover,
                                          height: 40,
                                          width: 40,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Container(
                                    child: Text(
                                      'Nike',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(60)),
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSanxDU2ph7m8Zf_u0NHNTa92_Be2oY-zMJmw&usqp=CAU',
                                fit: BoxFit.cover,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(60)),
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTz20xgpz7828OqeqI6JuA4YNtquFK5P9K_zA&usqp=CAU',
                                fit: BoxFit.cover,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(60)),
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVEj3nDAaNfnjfUJ5RwOFDH1UpTuoLRJGOLQ&usqp=CAU',
                                fit: BoxFit.cover,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(60)),
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              child: Image.network(
                                ' https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnab8BSbO078_bfwj1gaITQEsZkzTJlrkAVw&usqp=CAU',
                                fit: BoxFit.cover,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(60)),
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpYnmVz1vbsJyfOPvb9JvacWv2FPgiPYGGU-ZdWzWZYZwxdQdVkUkc7B6mXR_RYfBRJCw&usqp=CAU',
                                fit: BoxFit.cover,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          'Popular Shoes',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: Text(
                          'See all',
                          style: TextStyle(color: Colors.blue),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [

                  GestureDetector(
onTap: (){
  Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductDetailsPage()));
},
                  child: Container(
                  height: 160,
                    width: 130,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                    child: Column(
                      children: [
                        FittedBox(
                          child: Container(
                            child: Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-biQt02SPN3PsBKRJ1RbDSMWaFLVXSG8-6w&usqp=CAU'
                            ),
                            height: 100,
                            width: 100,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 15),
                          child: Text(
                            'BEST SELLER',
                            style:
                            TextStyle(color: Colors.blue, fontSize: 10),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),

                        Container(
                          margin: EdgeInsets.only(right: 15),
                          child: Text(
                            'Nike jordan',
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 12),
                          ),
                        ),
                        SizedBox(
                          height: 1,
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 38, left: 23),
                              child: Center(
                                child: Text(
                                  '\$493',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12),
                                ),
                              ),
                            ),
                            Container(
                              height: 26,
                              width: 25,
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(8),
                                      topLeft: Radius.circular(15))),
                              child: Icon(Icons.add),
                            ),
                          ],
                        ),


                      ],
                    ),
                  ),
        ),

                      GestureDetector(

                        child: Container(
                          height: 160,
                          width: 130,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          child: Column(
                            children: [
                              FittedBox(
                                child: Container(
                                  child: Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGhWrR7LnbhNLTNIhVXxcle2rWD25uTlrNLA&usqp=CAU'
                                  ),
                                  height: 100,
                                  width: 100,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 15),
                                child: Text(
                                  'BEST SELLER',
                                  style:
                                  TextStyle(color: Colors.blue, fontSize: 10),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),

                              Container(
                                margin: EdgeInsets.only(right: 15),
                                child: Text(
                                  'Nike Air Max',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400, fontSize: 12),
                                ),
                              ),
                              SizedBox(
                                height: 1,
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 38, left: 23),
                                    child: Center(
                                      child: Text(
                                        '\$897.99',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 26,
                                    width: 25,
                                    decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(8),
                                            topLeft: Radius.circular(15))),
                                    child: Icon(Icons.add),
                                  ),
                                ],
                              ),


                            ],
                          ),
                        ),
                      )




        ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          'New Arriveals',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: Text(
                          'See all',
                          style: TextStyle(color: Colors.blue),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // new Arriveals

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Container(
                        height: 120,
                        width: 300,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(15))

                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(padding: EdgeInsets.only(left: 10,
                                right: 20)),
                            Container(
                              margin: EdgeInsets.only(right: 20,left: 10,top: 10),
                              child:Text('BEST CHOICE',style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 10
                              ),) ,
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text('Nike Air Jordan'),
                                ),

                                SizedBox(width: 40,),

                                FittedBox(
                                  child: Container(


                                    child: Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQyXRzQ-SDEMr5Q6ixxo7exw6CsP70qyAxzA&usqp=CAU',
                                      height: 60,


                                    ),
                                  ),


                                ),


                              ],
                            ),
                            SizedBox(height: 5,),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text('\$567.99'),
                            )
                          ],
                        ),
                      )
                    ],
                  ),

                ],


              ),],
            ),
          ),
        ),
      ),
    );
  }
}





