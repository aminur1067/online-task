import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../home_page.dart';

class ProductDetailsPage extends StatefulWidget {
  const ProductDetailsPage({Key? key}) : super(key: key);

  @override
  State<ProductDetailsPage> createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Scaffold(
          backgroundColor: Colors.white70,
          body: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Route route =
                            MaterialPageRoute(builder: (context) => HomePage());
                        Navigator.pushReplacement(context, route);
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Container(
                          child: Icon(Icons.arrow_back),
                          height: 20,
                          width: 20,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          child: Text('Mens Shose'),
                        ),
                        Row(
                          children: [],
                        )
                      ],
                    ),
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Container(
                        child: Icon(Icons.lock_outlined),
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
                FittedBox(
                  child: Container(
                    child: Image.asset(
                      'assets/images/Captureuuu.JPG',
                      height: 180,
                      width: 340,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 366,
                  width: 360,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Column(
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 10, right: 270),
                          child: Text(
                            'BEST SELLER',
                            style: TextStyle(fontSize: 9, color: Colors.blue),
                          )),
                      Container(
                          margin: EdgeInsets.only(top: 10, right: 240),
                          child: Text(
                            'Nike Air Jordan',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 10, right: 280),
                          child: Text(
                            '\$967.98',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 10, left: 10),
                          child: Text(
                            'Air Jordan is an American bran of basketball'
                            'Shoes athletic, casual,and style clothing'
                            'produced by Nike ',
                            style: TextStyle(fontSize: 15),
                          )),

                      Container(
                          margin: EdgeInsets.only(top: 10, right: 270),
                          child: Text(
                            ' Gallery',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 25),
                          )),

                      SizedBox(
                        height: 5,
                      ),

                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqGEJwPv0yvCQiSBVqabHb7xW2mJFh4sRfow&usqp=CAU'),
                            height: 50,
                            width: 50,
                          ),
                          SizedBox(
                            width: 9,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR77UADgJmklv0euwZkXs5x78nvOJih9I5KA&usqp=CAU'),
                            height: 50,
                            width: 50,
                          ),
                          SizedBox(
                            width: 9,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-CWpPfYkKNSXVcKrW8RzhPznYrGTCNBm4jw&usqp=CAU'),
                            height: 50,
                            width: 50,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              '  Size',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          SizedBox(
                            width: 200,
                          ),
                          Container(
                            child: Text(
                              'EU',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Text(
                              'US',
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Text(
                              'UK',
                              style: TextStyle(fontSize: 12),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 20,),

                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child:  Center(child: Text('38')),
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(width: 10,),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child:  Center(child: Text('39')),
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(width: 10,),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child:  Center(child: Text('40')),
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(width: 10,),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child:  Center(child: Text('41')),
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(width: 10,),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child:  Center(child: Text('42')),
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(width: 10,),
                        ],
                      ),
                      SizedBox(height: 5,),
                      Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('Price'),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('\$849,99'),
                              ),
                            ],
                          ),
                          SizedBox(width: 180,),

                          Container(
                            height: 35,
                            width: 110,

                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.only(topRight: Radius.circular(15),
                                  topLeft: Radius.circular(15),bottomLeft: Radius.circular(15),bottomRight: Radius.circular(15))
                            ),
                            child: Center(child: Text('Add to Card')),

                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
