 import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';

import '../home_page.dart';
import 'menu_screen.dart';


class Draw2er extends StatelessWidget {

  Color p = Color(0xff416d69);
  final ZoomDrawerController z = ZoomDrawerController();

  @override
  Widget build(BuildContext context) {
    return ZoomDrawer(
      controller: z,
      borderRadius: 24,
      style:DrawerStyle.defaultStyle,
      showShadow: true,
      closeCurve: Curves.bounceIn,
      openCurve: Curves.fastOutSlowIn,
      slideWidth: MediaQuery.of(context).size.width * 0.65,
duration:Duration(microseconds: 500),
      angle: 0.0,
      menuBackgroundColor: Colors.black,
      mainScreen:  HomePage(),
      menuScreen: Theme(
        data: ThemeData.dark(),
        child: Scaffold(
          body:   ListView(
        children: [
        //header
        Container(
          margin: EdgeInsets.only(top: 80),
        child: Column(
          children: [
          Container(
          height: 80,
          width: 80,
          child: CircleAvatar(

            backgroundImage: NetworkImage(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8c0WMdYtZfsNr36x2365D8JCAhI4dFY95Iw&usqp=CAU'),
          ),
        ),
        SizedBox(
          height: 20,

        ),
        Text('Alison baker',style: TextStyle(
            color: Colors.grey,
            fontSize:24,
            fontWeight: FontWeight.bold
        ),)
        ],
      ),
    ),


    //body
    Container(
    padding: EdgeInsets.only(top: 1),
    child: Column(
    children: [
      ListTile(
        leading: Icon(Icons.person,color: Colors.grey,)
        ,
        title: Text(
          'Home Page',style: TextStyle(
            color: Colors.grey
        ),
        ),
        onTap: (){},

      ),
    ListTile(
    leading: Icon(Icons.home,color: Colors.grey,)
    ,
    title: Text(
    'Home Page',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),


    //my order
    ListTile(
    leading: Icon(Icons.lock_outlined,color: Colors.grey,)
    ,
    title: Text(
    'My Card',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),


    //not yet receve order
    ListTile(
    leading: Icon(Icons.favorite,color: Colors.grey,)
    ,
    title: Text(
    ' Fovorite',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),

    //Search
    ListTile(
    leading: Icon(Icons.delivery_dining,color: Colors.grey,)
    ,
    title: Text(
    'Order',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),

    //logout
    ListTile(
    leading: Icon(Icons.notifications,color: Colors.grey,)
    ,
    title: Text(
    'Notification',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),

    ListTile(
    leading: Icon(Icons.exit_to_app,color: Colors.grey,)
    ,
    title: Text(
    'Sing out',style: TextStyle(
    color: Colors.grey
    ),
    ),
    onTap: (){},

    ),

    //logout

    ],
    ),
    )
    ],
    ),
        ),
      ),
    );
  }
}


// class DrawerPagea extends StatelessWidget {
//   const DrawerPagea({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return ZoomDrawer(
//         menuScreen: DrawerPagea(),
//         mainScreen: MinSrceen(),
//     borderRadius: 24.0,
//     showShadow: true,
//     drawerShadowsBackgroundColor: Colors.grey,);
//   }
// }
//
//
//
// class MinSrceen extends StatelessWidget {
//
//   List<Listitem>Draweritem=[
//     Listitem(Icon(Icons.home),Text('home')),
//     Listitem(Icon(Icons.person),Text('Profile')),
//   ];
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.red,
//       body: Theme(
//         data: ThemeData.dark(),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children:  Draweritem.map((e) => ListTile(
//             title: e.title,
//             leading: e.icon,
//           )).toList(),
//         ),
//       ),
//     );
//   }
// }
//
// class Listitem {
//     final Icon icon;
//   final Text title;
//
//   Listitem(this.icon, this.title);
// }




